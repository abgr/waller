package api

import (
	"crypto/rand"
	"database/sql"
	"encoding/binary"
	"time"

	"gitea.com/anotherGo/aerr"

	_ "github.com/mattn/go-sqlite3"
)

type txLog struct {
	db *sql.DB
}
type TxLog interface {
	Transfer(from, to [64]byte, amount uint64, ttl int64) (txUUID []byte)
	Commit(txUUID []byte)
	Cancel(txUUID []byte)
	CreditCursor(account [64]byte) Cursor
	Clean()
}

func Open(str string) TxLog {
	db, err := sql.Open("sqlite3", str)
	aerr.Panicerr(err, nil)
	return txLog{db}
}
func newTxUUID() []byte {
	b := make([]byte, 32)
	_, err := rand.Read(b[8:])
	aerr.Panicerr(err, nil)
	binary.PutUvarint(b[:8], time.Now().UTC())
}
func (txLog txLog) Transfer(from, to [32]byte, amount uint64, ttl uint32) (txUUID []byte) {
	_, err := txLog.db.Exec("insert into `tx` (`uuid`,`from`,`to`,`amount`,`ttl`) values (?,?,?,?,current_timestamp)", from, to)
	aerr.Panicerr(err, nil)
	return nil
}

func (txLog txLog) Commit(txUUID []byte) {

}

func (txLog txLog) Cancel(txUUID []byte) {
	_, err := txLog.db.Exec("delete from `tx` where `uuid` = ? `ttl` is not null", txUUID)
	aerr.Panicerr(err, nil)
}

func (txLog txLog) Clean() {
	_, err := txLog.db.Exec("delete from `tx` where `ttl` < current_timestamp")
	aerr.Panicerr(err, nil)
}
func (txLog txLog) CreditCursor(account [64]byte) Cursor {
	rows, err := txLog.db.Query("select `uuid`,`from`,`to`,`amount`,`ttl` from `tx` where `from` = ? or `to` = ? order by `uuid` desc", account, account)
	aerr.Panicerr(err, nil)
	return cursor{rows}
}

type cursor struct {
	rows *sql.Rows
}
type Cursor interface {
	Next() (txUUID []byte, from, to [64]byte, amount uint64, ttl int64)
	Close()
}

func (cursor cursor) Next() (txUUID []byte, from, to [64]byte, amount uint64, ttl int64) {
	aerr.Panicerr(cursor.rows.Scan(&txUUID, &from, &to, &amount, &ttl), nil)
	return
}

func (cursor cursor) Close() {
	cursor.rows.Close()
}
